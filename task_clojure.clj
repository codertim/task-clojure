(ns my-stuff
  (:require clojure.contrib.str-utils))
(import '(java.awt.event ActionListener))
(use 'clojure.contrib.str-utils)
(use '[clojure.contrib.duck-streams :only (spit)])
(use '[clojure.xml :only (parse)])


; TO DO: notes in list for sorting
; :only in import
; nested nodes


(defn save-notes-xml [tm]
    (def all-notes-str "<notes>\n")
    (def root-node (.getRoot tm))
    (def nodes-length (.getChildCount root-node))
    (loop [i 0]
        (if (< i nodes-length)
            (do
                (println (str "NOde:  " i))
                (def all-notes-str (str all-notes-str "  <note>" (.getChildAt root-node i) "</note>\n"))
                (recur (inc i))
             )))
    (def all-notes-str (str all-notes-str "</notes>\n"))
    (spit "task_notes.xml" all-notes-str))


(defn save-notes [tm]
    (def all-notes "")
    (def root-node (.getRoot tm))
    ; (def root-node (.getChild tm 0))
    (def num-nodes (.getChildCount root-node))
    (println "Number of nodes:" num-nodes)
    (loop [i 0]
        (if (< i num-nodes)
            (do
                (println (str "Node: " i))
                (def all-notes (str all-notes (.getChildAt root-node i) "\n"))
                (recur (inc i) )
            )))
     ; (println (str "all-notes: " all-notes))
     ; now save to file
     ; (spit "task_notes.xml" all-notes)
     (save-notes-xml tm))


(defn make-menu [mb tm]
  (def save-item (new javax.swing.JMenuItem "Save"))
  (def quit-item (new javax.swing.JMenuItem "Exit"))
  (def file-menu (javax.swing.JMenu. "File"))
  (.add file-menu save-item)
  (.add file-menu quit-item)
  (.add mb file-menu)
  (. save-item
      (addActionListener
           (proxy [ActionListener] []
                (actionPerformed [evt]
                     (println "Saving ...")
                     (save-notes tm)))))
  (. quit-item
      (addActionListener
           (proxy [ActionListener] []
                (actionPerformed [evt]
                     (println "Exiting ...")
                     (System/exit 0)))) ))


(defn get-notes [my-file]
  "Retrieve notes from text file"
  (def parsed-xml (parse (java.io.File. my-file)))
  (println "\nParsed xml ...\n" parsed-xml)
  (def regular-seq (seq parsed-xml))
  (println "***** regular-seq" regular-seq)
  (def seq-as-xml-seq (xml-seq parsed-xml))
  (println "***** seq-as-xml-seq" seq-as-xml-seq)
  ; (seq seq-as-xml-seq)
  (seq (for [x seq-as-xml-seq
             :when (= :note (:tag x))] 
             (get (get x :content) 0))))


;(defn make-tree [] 
(def root (javax.swing.tree.DefaultMutableTreeNode. "Tasks"))
;  (def subroot (javax.swing.tree.DefaultMutableTreeNode. "SubRoot"))
;  (def leaf1 (javax.swing.tree.DefaultMutableTreeNode. "Leaf 1"))
;  (def leaf2 (javax.swing.tree.DefaultMutableTreeNode. "Leaf 2"))
(def tree-model (javax.swing.tree.DefaultTreeModel. root))
;  (.insertNodeInto tree-model subroot root 0)
;  (.insertNodeInto tree-model leaf1 subroot 0)
;  (.insertNodeInto tree-model leaf2 root 1)
(def tree (javax.swing.JTree. tree-model))
(.setEditable tree true)

(def notes (sort(get-notes "task_notes.xml")))
(println "Tasks=")
(println notes)


(loop [current-notes (reverse notes)]
  (def current-note (first current-notes))
  (do 
    (println "Current task=" current-note)
  )
  (if (empty? current-note)
    nil 
    (do
      (def new-node (javax.swing.tree.DefaultMutableTreeNode. current-note))
      (.insertNodeInto tree-model new-node root 0)
      (recur (rest current-notes)))))


(def menu-bar (new javax.swing.JMenuBar))
(make-menu menu-bar tree-model)


(doto (javax.swing.JFrame.)
  (.setLayout (java.awt.GridLayout. 2 2 3 3))
  (.add tree)
  (.setJMenuBar menu-bar)
  (.setSize 300 400)
  (.setTitle "Task Clojure")
  ; (.pack)
  (.setVisible true))


